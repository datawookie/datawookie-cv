https://github.com/nstrayer/cv/
https://github.com/nstrayer/datadrivencv

## JSON Resume

Uses https://www.npmjs.com/package/resume-cli.

Available themes can be found [here](https://jsonresume.org/themes/).

```
npm init --yes
npm install resume-cli jsonresume-theme-stackoverflow
```

```
# Default theme.
resume export resume.pdf

resume export resume-stackoverflow.pdf --theme=stackoverflow
resume export resume-stackoverflow.html --theme=stackoverflow

resume export resume-onepage-plus.pdf --theme=onepage-plus
resume export resume-elegant.pdf --theme=elegant
resume export resume-onepage-plus.pdf --theme=onepage-plus
resume export resume-onepage-plus.pdf --theme=onepage-plus

resume export resume-wookie.pdf --theme=wookie
resume export resume-wookie.html --theme=wookie
```

If you are running `resume` locally you might need to precede these commands with `npx`.

## Running Locally

Create a container.

```
docker run --rm -it --name resume -v $PWD:/resume datawookie/resume-cli
```

```
docker exec -it resume /bin/bash
```

Install packages.

```
npm install datawookie/jsonresume-theme-wookie
```

```
resume export resume-collier-andrew.pdf --theme=wookie
resume export resume-collier-andrew.html --theme=wookie
```

```
make collier-andrew-data-scientist.pdf
```
