Andrew holds a Masters Degree in Nuclear Physics (North-West University, South Africa), and a PhD in Space Physics (KTH Royal institute of Technology, Sweden).
 
Having worked on all seven continents, and with experience in academic and commercial environments, Andrew has refined his skills in research, data analysis and scientific computing. Equally importantly, Andrew has honed his ability to teach and communicate, ensuring his knowledge and insights are conveyed in an intuitive and easily accessible way to experts and laymen alike. Technical content in plain English.
 
Outside of work, Andrew continues to play with computers and data. He also enjoys cooking and gardening (and the outdoors in general). He’s a keen runner (both road and trail) and has slogged his way to a Comrades Marathon Green Number.
