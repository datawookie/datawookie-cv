GPP = gpp

PREFIX = collier-andrew
TYPES = data-scientist python-developer quantitative-developer r-developer web-scraping

PDF := $(shell for type in $(TYPES); do echo $(PREFIX)-$${type}.pdf; done) collier-andrew.pdf
HTML := $(shell for type in $(TYPES); do echo $(PREFIX)-$${type}.html; done) collier-andrew.html

clean:
	rm -f resume-*.json $(PREFIX)*.pdf $(PREFIX)*.html

all: $(PDF) $(HTML)

%.pdf: %.json
	resume export -r $< $@ --theme=wookie

%.html: %.json
	resume export -r $< $@ --theme=wookie

$(PREFIX)-r-developer.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DR_DEVELOPER | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX)-python-developer.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DPYTHON_DEVELOPER | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX)-web-scraping.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DWEB_SCRAPING | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX)-quantitative-developer.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DQUANTITATIVE_DEVELOPER | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX)-data-scientist.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DDATA_SCIENTIST | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX)-data-scientist.json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) -DDATA_SCIENTIST | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@

$(PREFIX).json: resume.json
	sed 's/\\n/\\\\n/g' $< | $(GPP) | perl -0777 -pe 's/<<#\n//g;s/\n>>#//g' >$@
